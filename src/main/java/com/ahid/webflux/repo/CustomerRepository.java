package com.ahid.webflux.repo;

import com.ahid.webflux.model.Customer;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CustomerRepository {
	Mono<Customer> getCustomerById(Long id);
	Flux<Customer> getAllCustomers();
	Mono<Void> saveCustomer(Mono<Customer> customer);
	Mono<Customer> putCustomer(Long id, Mono<Customer> customer);
	Mono<String> deleteCustomer(Long id);
}
